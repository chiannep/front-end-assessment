# The Branding Farm, Front End Coding Assessment

In this assessment, we're asking you to cut up a PSD and build a responsive static page. We provide you the PSD and some direction, you code the hell out of it. Get as fancy and technical as you want...We want to see how you think and solve problems.  

## What we're looking for

* Semantic, maintainable, easy to read code
* Use of future friendly, progressive enhancement techniques (SVG, icon fonts, CSS3 animations/transitions, minimal use of images, etc)

## Directions

* Fork or clone this repo
* Build static responsive page from the provided PSDs
* Check in (if you forked) or zip your completed site (if you cloned)
* Email us a link to your repo or the zip file